#!/usr/bin/php
<?php
#XTD:xhynek09
/* 
** Projekt 1 - 09: XTD: XML2DDL v PHP 5 pro IPP 2015/2016
** Jmeno a prijemni: Tomas Hynek (c) 2016; Login: xhynek09
*/
mb_internal_encoding("UTF-8");

define("YES", 1);
define("NO", 0);
define("BIT", 10);
define("INT", 20);
define("FLOAT", 30);
define("NVARCHAR", 40);
define("NTEXT", 50);
define("EMPTY_VALUE", 0);
define("UNKNOWN", 0);
define("UNSET_PARAM", -1);
define("ONE_N", -1);
define("N_ONE", -2);
define("N_M", -3);
define("N_ONE_SPECIAL", -4);


global $header, $fopen_out, $regex_numbers, $regex_integer, $regex_float, $param_a, $param_b, 
       $param_etc, $etcTable, $param_g;

$in = 0;
$out = 0;
$etcTable = array();
$printable = NO;
$param_etc = UNSET_PARAM;
$param_a = $param_b = $param_g = $param_header = false;
$header = "";
$input_text = "";
$regex_numbers = "/^[[:digit:]]+$/";
$regex_integer = "/^[+-]?[[:digit:]]+$/";
$regex_float = "/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/";

//###################################################################################
//############################# HELP - NAPOVEDA #####################################
function help() {
    echo "________________________________________________________________\n",
         "############### Projekt 1 - XTD v PHP 5.3.3 ####################\n",
         "############### Autor: Tomas Hynek (c) 2016 ####################\n",
         "###############       Login: xhynek09       ####################\n",
         "###############     Napoveda ke skriptu     ####################\n",
         "## --help\t\t- vypise na standardni vystup napovedu skriptu,\n",
         "## --input=filename\t- zadany vstupni soubor ve formatu XML,\n",
         "## --output=filename\t- zadany vystupni soubor ve formatu dle zadani,\n",
         "## --header='hlavicka'\t- na zacatek vystupniho souboru se vlozi zakomentovana hlavicka,\n",
         "## --etc=n\t\t- pro n>=0 urcuje maximalni pocet sloupcu vzniklych ze stejnojmennych podelementu,\n",
         "## -a\t\t\t- nebudou se generovat sloupce z atributu ve vstupnim XML souboru,\n",
         "## -b\t\t\t- pokud bude element obsahovat vice podelementu stejneho nazvu, bude se uvazovat,\n\t\t\t  jako by zde byl pouze jediny takovy (nelze kombinovat s parametrem --etc=n),\n",
         "## -g\t\t\t- lze jej uplatnit v kombinaci s jakymikoliv jinymi prepinaci vyjma --help,\n\t\t\t  pri jeho aktivaci bude vystupni soubor ve specialnim XML tvaru.\n",
         "________________________________________________________________\n";
}
/* ################################################################################### */
/* ################################################################################### */
/* Funkce vraci data typ ve forme cisla */
function getTypeNumber($string) {
   switch ($string) {
      case "BIT":       return BIT; break;
      case "INT":       return INT; break;
      case "FLOAT":     return FLOAT; break;
      case "NVARCHAR":  return NVARCHAR; break;
      case "NTEXT":     return NTEXT; break;
   }
}
/* Funkce vraci data typ ve forme stringu */
function getTypeName($string) {
   switch ($string) {
      case 10:     return "BIT"; break;
      case 20:     return "INT"; break;
      case 30:     return "FLOAT"; break;
      case 40:     return "NVARCHAR"; break;
      case 50:     return "NTEXT"; break;
   }
}
/* Funkce vraci datovy typ vstupnich dat a zaroven uvazuje prioritu mezi jednotlivymi dat. typy */
function getDataType($data, $attribute, $actualType) {
   global $regex_numbers, $regex_integer, $regex_float;

   $data = trim($data); // odstraneni mezer
   
   if (($data == "1") || ($data == "0") || ($data == "true") || ($data == "false") || (strlen($data) == EMPTY_VALUE)) {
      if ($actualType < BIT)  return "BIT";
      else  return getTypeName($actualType);
   } else if (preg_match_all($regex_integer, $data, $matches_out)) {
      if ($actualType < INT)  return "INT";
      else  return getTypeName($actualType);
   } else if (preg_match_all($regex_float, $data, $matches_out)) { //echo "### -> ".$data."\n";
      if ($actualType < FLOAT)  return "FLOAT";
      else  return getTypeName($actualType);
   } else {
      /* Pokud se jedna o data z atributu */
      if ($attribute == YES) {      
         if ($actualType < NVARCHAR)  return "NVARCHAR";
         else  return getTypeName($actualType);
      } else  {
         if ($actualType < NTEXT)  return "NTEXT";
         else  return getTypeName($actualType);
      }
   }
}
/* ################################################################################### */
/* ################################################################################### */
/* Funkce pro vytvoreni asociativniho pole, ktere znazornuje tabulky, jejich atributy a podelementy,
   datove typy, pocet vyskytu danych elementy a jednotlive vzajmene vztahy mezi tabulkami */
function createArrayOfTables(&$xml_arr, &$table) {
   global $fopen_out, $param_a, $param_g;
  
   /* Rekurzivni prochazeni objektu xml pro zpracovani do pole */
   foreach ($xml_arr->children() as $parent) {
      $parentName = (strtolower($parent->getName()))."_id";

      /* Podminka pro kontrolu zda byl zadan parametr -a */
      if ($param_a == false) {
         /* Pole pro atributy */
         $attributes = array();
         
         /* Kontrola zda uz existuji nejake podelementy k danemu objektu, pokud ano, bude se pokracovat ve vkladani */
         if (isset($table[$parentName]["@attributes"])) {
            $attributes = $table[$parentName]["@attributes"];
         }

         /* Prochazeni atributu objektu a ulozeni do pole */
         foreach ($parent->attributes() as $attribute) {
            $attributeName = strtolower($attribute->getName());
            $actualType = 0;

            /* Pokud atribut jiz existuje, vytahne se z nej jeho datovy typ*/
            if (isset($attributes[$attributeName])) 
               $actualType = getTypeNumber($attributes[$attributeName]);

            /* Pokud dany element jiz obsahuje data @value, porovnej zda se vkladany 
                a existujici podelement nejmenuji stejne a podle toho vloz do pole nove hodnoty  */
            if (isset($table[$parentName]["@value"])) {
               if (strcmp($attributeName, "value") == 0) {
                  $valueType = getTypeNumber($table[$parentName]["@value"]);
                  $table[$parentName]["@attributes"]["value"] = "NONE";
                  
                  /* Pokud to je atribut, oznam to dale funkci getDataType */
                  if (strcmp($table[$parentName]["@value"], "NTEXT") == 0) {
                     $type_if = NO;
                  } else {
                     $type_if = YES;
                  }

                  /* Vloz nebo pretypuj existujici podelement dle precedence dat. typu */
                  $table[$parentName]["@value"] = getDataType(strtolower($attribute), $type_if, $valueType);
               }
            } else {
               /* Vyhodnocuje se znovu datovy typ a podle priorit se nastavy novy */
               $attributes[$attributeName] = getDataType(strtolower($attribute), YES, $actualType);
            }
         }
        
         /* Ulozeni pole atributu k prislusnemu objektu */
         $table[$parentName]["@attributes"] = $attributes;
      }

      /* Pole pro pocet podelementu */
      $childrens = array();

      /* Rekurzivni volani pro zanoreni do podelementu */
      if ($parent->children()) {
         createArrayOfTables($parent, $table);
      }
      
      /* Pokud element nemá další podelementy, bude obsahovat value 
         Ale pokud atribut je taky value, nevytvari se novy sloupec ale pouze se vyhodnoti datovy typ */
      if (@count($parent->children()) == 0) {
         if ((strlen(trim((string)$parent[0]))) != 0) {
            $valueType = UNKNOWN;

            /* Kontrola kvuli kolizi atributu value a podelelety obsahujici @value */
            if (isset($table[$parentName]["@attributes"]["value"])) {
               if (strcmp($table[$parentName]["@attributes"]["value"], "NONE") == 0) {
                  if (isset($table[$parentName]["@value"])) {
                     $valueType = getTypeNumber($table[$parentName]["@value"]);
                  }
               } else if (isset($table[$parentName]["@attributes"]["value"])) {
                  $valueType = getTypeNumber($table[$parentName]["@attributes"]["value"]); 
               } else {
                  $valueType = UNKNOWN;
               }
            }            

            /* Vloz dany typ dat do tabulky */
            $table[$parentName]["@attributes"]["value"] = "NONE";
            $table[$parentName]["@value"] = getDataType(strtolower($parent[0]), NO, $valueType);
         }
      /* Pokud parent ma dalsi podelementy, zpracuji se */
      } else {
         /* Po vynorovani z rekurze se prochazi vsechny podelementy a zjistuje se kolize jmen, jejich pocet a datovy typ*/
         foreach ($parent->children() as $child) {
            $childName = (strtolower($child->getName()))."_id";
            $lowChildName = strtolower($childName);
            
            /* Pokud nazev atributu je stejny jako nazev nektereho sloupce -> chyba 90 */
            if (isset($table[$parentName]["@attributes"][$lowChildName])) {
               file_put_contents('php://stderr', "Error - kolize jmen (atributu, podelementu)\n");
               exit(90);
            }

            /* Pokud element nemá další podelementy, bude obsahovat value 
               Ale pokud atribut je taky value, nevytvari se novy sloupec ale pouze se vyhodnoti datovy typ */
            if (@count($child->children()) == 0) {
               if ((strlen(trim((string)$child))) != 0) {
                  if (isset($table[$childName]["@attributes"]["value"])) {
                     $valueType = getTypeNumber($table[$childName]["@attributes"]["value"]);
                     $table[$childName]["@attributes"]["value"] = "NONE";
                     $table[$childName]["value"] = getDataType(strtolower($child), NO, $valueType);
                  } else {
                     $table[$childName]["@value"] = getDataType(strtolower($child), NO, UNKNOWN);
                  }
               }
            }
            /* Ukladani poctu vyskytu daneho podelementu pro pozdejsi pouziti napriklad s parametrem etc */
            if (isset($childrens[$childName])) {
               $childrens[$childName]++;
            } else {
               if ((((strcmp($parentName, $childName)) !== 0) && ($param_g == true)) || ($param_g !== true)) {
                  $childrens[$childName] = 1;
               }
            }
         }
      }
      
      /* Pole pro podelementy */
      $children = array();

      /* Ukladani vsech podelementu k danemu elementu do pole,
         Pokud jiz nejake podelementy element obsahuje, pokracuje se v ukladani */
      if (isset($table[$parentName]["@subelements"])) {
         $children = $table[$parentName]["@subelements"];

         /* Prochazi se pocty podelemetu k danemu elementu, pokud je cislo vetsi, ulozi se novy max. stav */
         foreach ($childrens as $index => $childrenCount) {
            if (isset($children[$index])) {
               if ($children[$index] < $childrenCount)
                  $children[$index] = $childrenCount;

            } else {
               $children[$index] = $childrenCount;
            }
         }
         $table[$parentName]["@subelements"] = $children;
      } else {
         $table[$parentName]["@subelements"] = $childrens;  
      }
   }
}
/* ################################################################################### */
/* ################################################################################### */
/* Vypsani jednotlivych tabulek dle asociativniho pole a pravidel v zadani */
function getDDLTable(&$tables) {
   global $fopen_out, $param_a, $param_b, $param_g, $param_etc;

   /* Prochazeni jednotlivych poli, ktere obsahuje tabulky a jejich sloupce */
   foreach($tables as $tableName => $subelement) {
      $explodedTable = explode("_id", $tableName);
      $tableString = "CREATE TABLE ".$explodedTable[0]."(\n   prk_".$tableName." INT PRIMARY KEY";
      fprintf($fopen_out, "%s", $tableString);

      /* Pokud je nastaven parametr -a, nevypisuji se sloupce s atributy, naopak se vypisi i sloupce s atributy */
      if ($param_a == false) {
         if (isset($subelement["@attributes"])) {
            foreach ($subelement["@attributes"] as $attributeName => $attributeType) {
               if ($attributeType != "NONE") {
                  $attributeString = ",\n   ".$attributeName." ".$attributeType;
                  fprintf($fopen_out, "%s", $attributeString);   
               }
            }
         }
      }

      /* Pokud tabulka ma sloupce, zjisti jejich pocet a podle toho cisluje vypis jejich jmen */
      if (isset($subelement["@subelements"])) {
         foreach ($subelement["@subelements"] as $subelementName => $subelementData) {
            /* Pokud je vice stejnojmennych podelementu a neni nastaven parametr -b, kazdy znich ocisluj */
            if (($subelementData >= 2) && ($param_b !== true)) {
               for ($counter = 1; $counter <= $subelementData; $counter++) {
                  $explodedElement = explode("_id", $subelementName);
                  $subelementString = ",\n   ".$explodedElement[0].$counter."_id INT";
                  fprintf($fopen_out, "%s", $subelementString);
               }
            /* Neni vice stejnojmennych podelementy, vypis klasicky bez pridaneho cisla */
            } else {
               $subelementString = ",\n   ".$subelementName." INT";
               fprintf($fopen_out, "%s", $subelementString);
            }
         }
      }

      /* Pokud dana tabulka ma hodnotu value, vypis ji */
      if (isset($subelement["@value"])) {
         $valueString = ",\n   value ".$subelement["@value"];
         fprintf($fopen_out, "%s", $valueString);
      }

      fprintf($fopen_out, "\n);\n\n");
   }
}
/* ################################################################################### */
/* ################################################################################### */
/* Pokud byl nastaven parametr --etc, tak projdi celou tabulku a preskladej podle toho dane elementy tak,
   aby odpovidaly zadanemu cislu z parametru etc */
function reorderETC(&$tables) {
   global $param_etc, $etcTable;
   
   /* Prochazeni celeho asociativniho pole */
   foreach ($tables as $tableName => $subelement) {
      $subSubElements = array();

      /* Pokud tabulka ma nejake sloupce, pro kazdy zjisti zda jejich pocet neni vetsi nebo roven 
         parametru etc a podle toho preskladej dane sloupce do vysledny struktury */
      if (isset($subelement["@subelements"])) {
         foreach ($subelement["@subelements"] as $subelementName => $subelementData) {
            if ($param_etc >= $subelementData) {
               $subSubElements[$subelementName] = $subelementData;
            } else {
               $tables[$subelementName]["@subelements"][$tableName] = N_ONE_SPECIAL;
               $etcTable[$tableName][$subelementName] = $subelementData;
            }
         }

         $tables[$tableName]["@subelements"] = $subSubElements;
      }
   }
}
/* ################################################################################### */
/* ################################################################################### */
/* Funkce prochazi jednotlive tabulky a jejich sloupce a zjistuje jejich vzajemne vztahy
   a podle toho k nim zapisuje jejich vztahy 
   Vztahy:  ONE_N = 1:N
            N_ONE = N:1
            N_M   = N:N    */
function reorderParamG(&$tables) {
   /* Pro kazdou tabulku zjisti zda ma nejake sloupce az na sloupce atributu */
   foreach ($tables as $tableName => $table) {
      $position = 0;
      $relationTable = array();

      /* Pokud ma sloupec, tak pro kazdy z nej zjisti kolikkrat se tento podelement v elementu vyskytl */
      if (isset($table["@subelements"])) {
         foreach ($table["@subelements"] as $parentName => $parent) {
            if ($parent > 0) {
               $relationTable[$position] = $parentName;
               $position++;

               /* Jesltize ma i dany podelementy zaznam ve vlastni tabulce o danem aktualnim rodicovskem elementu, 
                  tak zjisti zda jiz neexistuje jejich vztah a zda tento novy vztah ma vetsi vahu */
               if (isset($tables[$parentName]["@subelements"][$tableName])) {
                  if (!(($tables[$tableName]["@subelements"][$parentName] > 0) && ($tables[$parentName]["@subelements"][$tableName] > 0)))
                     $tables[$parentName]["@subelements"][$tableName] = ONE_N;
               } else {
                  $tables[$parentName]["@subelements"][$tableName] = ONE_N;
               }
            }
         }

         $i = 0;

         /* Dale rekurzivne prochazej vsechny tabulky aktualnich sloupcu (podelementu) v aktualni tabulce a prochazej
            vsechny vyskyty v poli relationTable a dolnuj jejich vztahy */
         while (isset($relationTable[$i])) {
            $newTable = $relationTable[$i];
               /* Pokud ma sloupec, tak pro kazdy z nej zjisti kolikkrat se tento podelement v elementu vyskytl */
               if (isset($tables[$newTable]["@subelements"])) { 
                  foreach ($tables[$newTable]["@subelements"] as $childName => $child) {
                     if ($child > 0) {
                        /* Pokud neni sloupce stejny jako jmeno tabulky, vloz ji do pole pro zpracovani */
                        if ((strcmp($childName, $tableName)) !== 0) {
                           $relationTable[$position] = $childName;
                           $position++;
                        }
                     }
                  }

                  /* Pokud nektery ze sloupcu (zaznamu v poli) ma take zaznam u sebe v tabulce o tabulce,   
                     ve ktere se vyskytuje jako sloupec, tak je potreba porovnan jejich zavisloti a popripade prepsat novou zavislosti */
                  if (isset($tables[$tableName]["@subelements"][$newTable])) {
                     if (($tables[$tableName]["@subelements"][$newTable] > 0) && ($tables[$newTable]["@subelements"][$tableName] > 0)) {
                        $tables[$newTable]["@subelements"][$tableName] = N_M;
                        $tables[$tableName]["@subelements"][$newTable] = N_M;
                     } else {
                        $tables[$newTable]["@subelements"][$tableName] = ONE_N;
                     }
                  } else {
                     $tables[$newTable]["@subelements"][$tableName] = ONE_N;
                  }
               }

            $i++;
         }
      }

      /* Nakonec vsechny zapsane elementy v relationTable zapis do aktualnich zpracovavane tabulky a nastav jejich vztah N:1 */
      foreach ($relationTable as $childName => $child) {
         if (!isset($tables[$tableName]["@subelements"][$child]))
            $tables[$tableName]["@subelements"][$child] = N_ONE; 
      }
   }
}
/* ################################################################################### */
/* ################################################################################### */
/* Funkce doplnuje primarne vztahy N:M relaci */
function reorderParamG_MNRelation(&$tables) {
   /* Pro kazdou tabulku zjisti zda ma nejake sloupce az na sloupce atributu */  
   foreach ($tables as $tableName => $table) {
      $position = 0;
      $relationTable = array();

      /* Pokud ma tabulka sloupce a hodnota poctu vyskytu jmena slupce je vetsi jak nula, dopln vztah N:1 */
      if (isset($table["@subelements"])) {
         foreach ($table["@subelements"] as $parentName => $parent) {
            if ($parent > 0) {
               $relationTable[$position] = $parentName;
               $position++;
               
               $tables[$tableName]["@subelements"][$parentName] = N_ONE;
            }
         }

         $i = 0;

         /* Rekurzivne doplnuj relace N:M pro dane sloupce do jejich tabulek */
         while (isset($relationTable[$i])) { 
            $newTable = $relationTable[$i];

            /* Pokud ma tabulka sloupce, dopln vztah N:M do tabulky jejiz nazve se vyskytl v tabulce $table */
            if (isset($tables[$newTable]["@subelements"])) {
               foreach ($relationTable as $childKey => $childValue) { 
                  if ((strcmp($newTable, $childValue)) !== 0) { 
                     $tables[$newTable]["@subelements"][$childValue] = N_M;
                  }
               }
            }

            $i++;
         }
      }
   }
}
/* ################################################################################### */
/* ################################################################################### */
/* Funkce prochazi jednotlive tabulky a kontroluje zda nejaky element neobsahuje N_ONE_SPECIAL
   a pokud ano, tak podle toho dplnuje spravne vztahy relace */
function reorderParamG_Special(&$tables) {
   global $etcTable;

   /* Pro kazdou tabulku zjisti zda ma nejake sloupce az na sloupce atributu */  
   foreach ($tables as $tableName => $table) {
      $special = false;

      $position = 0;
      $relationTable = array();  // inicializace pole 

      $relationTable[$position] = $tableName;   // ulozeni jmena tabulky do pole
      $position++;
      
      /* Pokud ma sloupce, tak zjisti zda dany sloupec obsahuje hodnotu N_ONE_SPECIAL (-4) */
      if (isset($table["@subelements"])) {
         foreach ($table["@subelements"] as $parentName => $parent) {   
            if ($parent == N_ONE_SPECIAL) { 
               $specialName = $parentName;
               $special = true;
            } else {
               $relationTable[$position] = $parentName;
               $position++;
            }
         }
      }

      /* Pokud byl nalezen nejaky sloupce se specialnim typem, zpracuj ostatni sloupce a dopln do nich relace */
      if ($special == true) {
         /* Pro kazdy sloupec, ktery specialni typ neobsahoval ale zaroven tabulka obsahovala sloupec se specialnim typem */
         foreach ($relationTable as $childName => $child) {
            /* Dopln nasledujici relace do tabulky se jmenen sloupce, ktery mel specialn typ */
            if (!isset($tables[$specialName]["@subelements"][$child])) {
               $tables[$specialName]["@subelements"][$child] = N_M;  
            }

            if (isset($etcTable[$specialName][$child])) {
               $tables[$specialName]["@subelements"][$child] = ONE_N; 
            }
         }

         /* Pote dopln nebo prepis vztahy na N:M ke do vsech tabulek,
            se kterymy se pracovalo vyse pro sloupec se specialnim typem */
         foreach ($tables as $tableName2 => $table2) {
            if (isset($table2["@subelements"][$specialName])) { 
               foreach ($relationTable as $childName => $child) { 
                  /* Pokud jeste neexistuje zaznam o danem sloupci childa sloupec se nejmenuje stejne jako tabulka, zapis relaci */
                  if (!isset($tables[$tableName2]["@subelements"][$child])) {
                     if ((strcmp($tableName2, $child)) !== 0) {
                        $tables[$tableName2]["@subelements"][$child] = N_M; 
                     }
                  }
               }
            }
         }
      }
   }
}
/* ################################################################################### */
/* ################################################################################### */
/* Funkce se vyuziva pouze pri pouziti parametru --etc a prochazi jednotlive tabulky,
   kde kontroluje zda jsou vsechny prvky ve spravne relaci, pokud ne, dle precedence 
   se prepisuji jednotlive vztahy mezi nimi */
function completeRelation(&$tables) {
   /* Pro kazdou tabulku zjisti zda ma sloupce */
   foreach ($tables as $tableName => $table) {
      if (isset($table["@subelements"])) {
         /* Prochazej jednotlive sloupce a zjistuj, zda tabulka s nazvem sloupce ma vztah k dane tabulce,
            ve ktere se prave vyskytuje a over jejich vztah a popriade oprav vztah */
         foreach ($table["@subelements"] as $parentName => $parentValue) {   
            if (!isset($tables[$parentName]["@subelements"][$tableName])) {
               if ($parentValue == ONE_N) {
                  $tables[$parentName]["@subelements"][$tableName] = N_ONE;
               } else if ($parentValue == N_ONE) {
                  $tables[$parentName]["@subelements"][$tableName] = ONE_N;
               } else {
                  $tables[$parentName]["@subelements"][$tableName] = N_M;
               }  
            }
         }
      }
   }
}
/* ################################################################################### */
/* ################################################################################### */
/* Funkce provadejici kompletovani N:M relaci */
function completeRelationN_M(&$tables) {
   /* Pro kazdou tabulku se provadi kompletace relace N:M */
   foreach ($tables as $tableName => $table) {
      /* Pokud ma tabulka sloupce, tak pro kazdy sloupec se kontroluje zda nema N:M relaci,
         pokud ano, tak se kontroluje v tabulce se jmenem sloupce, zda tento vztah nechybi nebo neni jiny 
         a podle toho se doplni ci opravi vztah */
      if (isset($table["@subelements"])) {   
         foreach ($table["@subelements"] as $parentName => $parentValue) {   
            /* Pokud je vtazh N:M, mel by byt tento vztah v relaci i z druheho pohledu */
            if ($parentValue == N_M) { 
               if (isset($tables[$parentName]["@subelements"])) { 
                  /* Kontrola vztahu u tabulky se jmenem sloupce */
                  foreach ($tables[$parentName]["@subelements"] as $tableKey => $tableValue) {
                     /* Pouze pokud je vztah N:1 a jmeno sloupce neni stejne jako jmeno tabulky */
                     if ($tableValue == N_ONE) { 
                        if ((strcmp($tableName, $tableKey)) !== 0) { 
                           if (!isset($tables[$tableName]["@subelements"][$tableKey])) {
                              $tables[$tableName]["@subelements"][$tableKey] = N_M;       // vlozeni vtahu N:M
                           }
                        }
                     }
                  }
               }  
            }

            /* Pokud se jedna o vztah 1:N, tak je potreba zjistit zda v tabulce se jmenem sloupce neni N:M
               a podle toho popripade doplnit i zde N:M */
            if ($parentValue == ONE_N) {
               if (isset($tables[$parentName]["@subelements"])) { 
                  foreach ($tables[$parentName]["@subelements"] as $tableKey => $tableValue) {
                     /* Pouze pokud je vztah N:M a jmeno sloupce neni stejne jako jmeno tabulky */
                     if ($tableValue == N_M) {
                        if ((strcmp($tableName, $tableKey)) !== 0) {
                           if (!isset($tables[$tableName]["@subelements"][$tableKey])) {
                              $tables[$tableName]["@subelements"][$tableKey] = N_M;
                           }
                        }
                     }
                  }
               }  
            }
         }
      }
   }
}
/* ################################################################################### */
/* ################################################################################### */
/* Dle preorganizovane a pretvorene tabulky funkce vypise jendotlive relace danych tabulek
   k danym sloupcum dle formatu v zadani */
function getTableRelations(&$tables) {
   global $fopen_out;

   $header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<tables>";    // uvodni radek
   $footer = "\n</tables>\n";                                           // posledni radek
   fprintf($fopen_out, "%s", $header);

   /* Pro kazdou tabulku vypis jeji relace */
   foreach ($tables as $tableName => $table) {
      $explodedTable = explode("_id", $tableName);

      $tableString = "\n\t<table name=\"".$explodedTable[0]."\">";      // tabulka
      fprintf($fopen_out, "%s", $tableString);

      $tableRelString = "\n\t\t<relation to=\"".$explodedTable[0]."\" relation_type=\"1:1\" />";   // relace tabulky k vlastnimu jmenu
      fprintf($fopen_out, "%s", $tableRelString);

      /* Pokud ma tabulka nejake sloupce, vypis k nim relace */
      if (isset($table["@subelements"])) { 
         foreach ($table["@subelements"] as $parentName => $parent) { 
            $explodedChild = explode("_id", $parentName);
            
            /* Dle hodnoty sloupce se vypise dany vztah k teto tabulce 
               ONE_N = -1;    N_ONE = -2;    N_M = -3 */
            if ($parent == ONE_N) {
               $childString = "\n\t\t<relation to=\"".$explodedChild[0]."\" relation_type=\"1:N\" />";
               fprintf($fopen_out, "%s", $childString);
            } else if (($parent == N_ONE) || ($parent == N_ONE_SPECIAL)){
               $childString = "\n\t\t<relation to=\"".$explodedChild[0]."\" relation_type=\"N:1\" />";
               fprintf($fopen_out, "%s", $childString);
            } else {
               $childString = "\n\t\t<relation to=\"".$explodedChild[0]."\" relation_type=\"N:M\" />";
               fprintf($fopen_out, "%s", $childString);
            }         
         }
      }

      $endOfTable = "\n\t</table>";    // ukonceni tabulky
      fprintf($fopen_out, "%s", $endOfTable);
   }

   fprintf($fopen_out, "%s", $footer);
}
/* _________________________________________________________________________________________________________ */
/* ********************************************************************************************************* */
/* ******************************************* HLAVNI PROGRAM ********************************************** */

/* ########################### ZPRACOVANI ARGUMENTU ################################## */
/* Obsahuje zaroven kontrolu jednolivych souboru, zda existuji, jdou otevrit, zda byl 
 * vubec zadan nebo zda bylo zadano dost nebo moc parametru */
for ($i = 1; $i < $argc; $i++) {
   /* ******** PARAMETER HELP ******** */
   if ((strpos($argv[$i], '--help')) !== false) {
      $help = explode("--help", $argv[$i]);  // ziskani hodnoty parametru
      
      if (($argc > 2) || ($help[1] !== "") || ($help[0] !== "")) {   // kontrola kombinace parametru 
         file_put_contents('php://stderr', "Error - spatne parametry\n");
         exit(1);     // spatny format parametru nebo zakazana kombinace parametru
      }

      help();         // konec skriptu bez chyby s napovedou
      exit(0);

   /* ******* PARAMETER --INPUT ******* */
   } else if ((strpos($argv[$i], '--input=')) !== false) {
      if ($in > 0) {    // pokud byl zadan parametr vicekrat, chyba
         file_put_contents('php://stderr', "Error - duplicita parametru\n");
         exit(1);
      }

      $input = explode("--input=", $argv[$i]);        // ziskani hodnoty parametru
      if (($input[1] == "") || ($input[0] !== "")) {  // kontrola zadanych parametru
         file_put_contents('php://stderr', "Error - spatne parametry\n");
         exit(1);    
      }

      $in += 1;                  // counter zadanych inputu
      $inputFile = $input[1];    // ulozeni nazvu souboru (cesty s nazvem)
      fclose(STDIN);

   /* ******* PARAMETER OUTPUT ******* */
   } else if ((strpos($argv[$i], '--output=')) !== false) {
      if ($out > 0) {   // pokud byl zadan parametr vicekrat, chyba
         file_put_contents('php://stderr', "Error - duplicita parametru\n");
         exit(1);
      }

      $output = explode("--output=", $argv[$i]);         // ziskani hodnoty parametru
      if (($output[1] == "") || ($output[0] !== "")) {   // kontrola zadanych parametru
         file_put_contents('php://stderr', "Error - spatne parametry\n");
         exit(1);                                           
      }

      $out += 1;                 // counter zadanych outputu
      $outputFile = $output[1];  // ulozeni nazvu souboru (cesty s nazvem)

   /* ******* PARAMETER HEADER ******* */
   } else if ((strpos($argv[$i], '--header=')) !== false) {   
      $head = explode("--header=", $argv[$i]);  // ziskani hodnoty parametru
      if ($head[0] !== "") {                    // kontrola zadaneho parametru
         file_put_contents('php://stderr', "Error - spatne parametry\n");
         exit(1);       
      }

      $param_header = true;
      $header = sprintf("--%s\n\n", $head[1]);  // ulozeni hlavicky

   /* ******** PARAMETER ETC ********* */
   } else if ((strpos($argv[$i], '--etc=')) !== false) {   
      $etc_arr = explode("--etc=", $argv[$i]);  // ziskani hodnoty parametru

      // kontrola kombinaci povolenych parametru dle zadani
      if (($etc_arr[1] == "") || ($etc_arr[0] !== "") || ($param_b !== false) || ($etc_arr[1] < 0)) {    
         file_put_contents('php://stderr', "Error - spatne parametry\n");
         exit(1);       
      }

      // kontrola, zda byla hodnota etc zadana ve spravnem formatu
      if (!preg_match_all($regex_numbers, $etc_arr[1], $matches_out)) {       
         file_put_contents('php://stderr', "Error - spatne parametry\n");
         exit(1);      
      }

      $param_etc = (int)$etc_arr[1];   // ulozeni hodnoty etc

   /* ********** PARAMETER A ********* */
   } else if ((strpos($argv[$i], '-a')) !== false) {   
      $param_a = true;     // indikace parametru -a

   /* ********** PARAMETER B ********* */
   } else if ((strpos($argv[$i], '-b')) !== false) {
      if ($param_etc !== UNSET_PARAM) {     // pokud byl jiz zadan parametr --etc, nelze kombinovat s -b
         file_put_contents('php://stderr', "Error - spatne parametry\n");
         exit(1);                                           
      }

      $param_b = true;     // indikace parametru -b

   /* ********* PARAMETER G ********** */
   } else if ((strpos($argv[$i], '-g')) !== false) {   
      $param_g = true;     // indikace parametru -g

   /* ******* SPATNE PARAMETRY ******* */
   } else {   
      file_put_contents('php://stderr', "Error - spatne parametry\n");
      exit(1);   
   }
}

/* ################################################################################### */
/* Pokud byl zadan vstupni soubor, otevri ho */
if ($in !== 0) {
   if(($fopen_in = fopen($inputFile, 'r')) == false) {
      file_put_contents('php://stderr', "Error - chyba pri pokusu otevreni vstupniho souboru\n");
      exit(2);
   }

   $xml_arr = simplexml_load_file($inputFile);  // nacti dany soubor pomoci SimpleXML
   if($xml_arr ===  false) {                    // pokud je zadan spatny vstup
      file_put_contents('php://stderr', "Error - soubor neexistuje\n");
      exit(2); 
   }
} else {
   if ($in == 0) {      
      $fopen_in = fopen("php://stdin", "r");
      
      /* Nacti vsechny znaky, co se vyskytnou na standartnim vstupu */
      while (($char = fgetc($fopen_in)) !== false) {
         $input_text .= $char;
      }
      
      $xml_arr = simplexml_load_string($input_text);  // nacti text pomoci SimpleXML
      if($xml_arr ===  false) {                       // pokud je zadan spatny vstup 
         file_put_contents('php://stderr', "Error - soubor neexistuje\n");
         exit(2); 
      }
   }
}

/* Pokud byl zadan vystupni soubor */
if ($out !== 0) {
   if(($fopen_out = fopen($outputFile, 'w')) == false) {
      file_put_contents('php://stderr', "Error - chyba pri pokusu otevreni souboru pro zapis\n");
      exit(3);
   }
} else {
   if ($out == 0)
   $fopen_out = fopen("php://stdout", "w");
}

/* Pokud byla na vstupu hlavicky, provede se zapis do vystupniho souboru */
if (($param_header == true) && ($param_g !== true)) {
   fprintf($fopen_out, "%s", $header);
}

/* ########################## ZPRACOVANI XML SOUBORU ################################# */
/* Vytvoreni asociativniho pole, ktere bude obsahovat tabulky, atributy, podelementy a jejich vzajemne vztahy */
$table = array();
createArrayOfTables($xml_arr, $table);

unset($xml_arr);

/* Zpracovani parametru etc a preskladani elementu tabulek */
if ($param_etc !== UNSET_PARAM)
   reorderETC($table);

/* ###################### ZPRACOVANI RELACI NEBO VYTTVORENI DDL ###################### */
/* Bud se maji vypsat CREATE TABLE tabulky (format DDL) nebo se vypisou relace tabulek */
if ($param_g == true) {
   /* Nasleduji funkce pro pretvoreni tabulky k vypsani relaci (podrobnejsi popis viz kod funkce) */
   reorderParamG($table);
   reorderParamG_MNRelation($table);
   reorderParamG_Special($table);
   
   if ($param_etc !== UNSET_PARAM)
      completeRelation($table);

   completeRelationN_M($table);

   /* Vypsani jednotlivych relaci */
   getTableRelations($table);
} else {
   /* Vypsani CREATE TABLE tabulek dle specifikaci v zadani */
   getDDLTable($table);
}

exit(0);
?>